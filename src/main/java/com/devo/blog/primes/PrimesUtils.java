package com.devo.blog.primes;

import java.util.Collection;
import java.util.Map;

/**
 * author: Anton Krasnov
 */
public class PrimesUtils {

    private PrimesUtils() {
    }

    public static void initNaturals(Map<Integer, Integer> naturals, int setLimit) {
        for (int i = 2; i <= setLimit; i++) {
            naturals.put(i, i);
        }
    }

    public static void initNaturals(Collection<Integer> naturals, int setLimit) {
        for (int i = 2; i <= setLimit; i++) {
            naturals.add(i);
        }
    }
}
