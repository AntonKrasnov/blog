package com.devo.blog.primes;

import com.devo.blog.primes.strategy.*;

/**
 * Research java structures based on finding primes case
 * <p/>
 * author: Anton Krasnov
 */
public class PrimesCounter {

    private static final int NUMBER_LIMIT = 100_000_000;

    private PrimesCounter() {
    }

    public static void main(String... args) {
        System.out.println("Max memory is " + Runtime.getRuntime().maxMemory() + '\n');

        PrimeCalculationStrategy strategy = new BitSetEratosthenesStrategy();
        calculatePrimes(strategy);

        strategy = new BitSetAtkinStrategy();
        calculatePrimes(strategy);

       strategy = new BooleanArrayEratosthenesStrategy();
        calculatePrimes(strategy);

        strategy = new BooleanArrayAtkinStrategy();
        calculatePrimes(strategy);

/*        strategy = new HashMapEratosthenesStrategy();
        calculatePrimes(strategy);

        strategy = new HashSetEratosthenesStrategy();
        calculatePrimes(strategy);*/
    }

    private static void calculatePrimes(PrimeCalculationStrategy strategy) {
        long startTime;
        long endTime;
        startTime = System.currentTimeMillis();
        int primesCount = strategy.getPrimesCount(NUMBER_LIMIT);
        endTime = System.currentTimeMillis();
        System.out.println("Process time for " + strategy.getStrategyName() + ": " + (endTime - startTime));
        System.out.println("Total primes: " + primesCount + '\n');
    }
}
