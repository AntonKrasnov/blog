package com.devo.blog.primes.strategy;

/**
 * Algorithm based on sieve of Eratosthenes and used array of boolean structure
 * <p/>
 * pros: good performance and friendly to use memory (but it depends on the CPU architecture and JVM - see cons for details)
 * <p/>
 * cons: boolean array elements take 1 byte instead of 1 bit is because (most) CPU architectures don't provide the ability
 * to directly read and write individual bits of memory. The smallest unit PCs can manipulate has 8 bits.
 * The JVM could pack the bits together, then to modify a bit it would read the byte, modify it, and write it back,
 * but that does not work if multiple threads are modifying the array simultaneously.
 * <p/>
 * author: Anton Krasnov
 */
public class BooleanArrayEratosthenesStrategy implements PrimeCalculationStrategy {

    private static final int FIRST_NUMBER = 2;

    @Override
    public int getPrimesCount(int numberLimit) {
        int primesCount = 0;
        Boolean[] booleans = initBooleans(numberLimit);

        int i = FIRST_NUMBER;
        int root = (int) Math.ceil(Math.sqrt(numberLimit));
        while (i  < root) {
            if (booleans[i]) {
                primesCount++;
                int multiple = i + i;
                while (multiple <= numberLimit) {
                    booleans[multiple] = false;
                    multiple += i;
                }
            }
            i++;
        }

        while (i <= numberLimit) {
            if (booleans[i]) {
                primesCount++;
            }
            i++;
        }
        return primesCount;
    }

    private static Boolean[] initBooleans(int numberLimit) {
        Boolean[] booleans = new Boolean[numberLimit + 1];
        for (int i = FIRST_NUMBER; i <= numberLimit; i++) {
            booleans[i] = true;
        }
        return booleans;
    }

    @Override
    public String getStrategyName() {
        return "Boolean array structure with sieve of Eratosthenes algorithm strategy";
    }
}
