package com.devo.blog.primes.strategy;

/**
 * Algorithm based on sieve of Atkin and used array of boolean structure.
 *
 * <p/>
 * pros: excellent performance. Atkin algorithm is more efficient than the Sieve of Eratosthenes and less demanding memory
 * <p/>
 * cons: java.util.BitSet more friendly to the memory
 * <p/>
 * author: Anton Krasnov
 */
public class BooleanArrayAtkinStrategy implements PrimeCalculationStrategy {

    @Override
    public int getPrimesCount(int numberLimit) {
        /* initialize the Atkin sieve */
        boolean[] prime = new boolean[numberLimit + 1];
        prime[2] = true;
        prime[3] = true;
        int root = (int) Math.ceil(Math.sqrt((double) numberLimit));

        /* put in candidate primes: integers which have an odd number of representations by certain quadratic forms */
        for (int x = 1; x < root; x++) {
            for (int multiple = 1; multiple < root; multiple++) {
                int n = 4 * x * x + multiple * multiple;
                if (n <= numberLimit && (n % 12 == 1 || n % 12 == 5)) {
                    prime[n] = !prime[n];
                }
                n = 3 * x * x + multiple * multiple;
                if (n <= numberLimit && n % 12 == 7) {
                    prime[n] = !prime[n];
                }
                n = 3 * x * x - multiple * multiple;
                if (x > multiple && n <= numberLimit && n % 12 == 11) {
                    prime[n] = !prime[n];
                }
            }
        }

        /* eliminate composites by sieving, omit multiples of its square */
        for (int i = 5; i <= root; i++) {
            if (prime[i]) {
                for (int j = i * i; j < numberLimit; j += i * i) {
                    prime[j] = false;
                }
            }
        }

        /* getting count primes */
        int primeCounts = 0;
        for (boolean isPrime : prime) {
            if (isPrime) {
                primeCounts++;
            }
        }
        return primeCounts;
    }

    @Override
    public String getStrategyName() {
        return "Boolean array structure with sieve of Atkin algorithm strategy";
    }
}