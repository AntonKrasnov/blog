package com.devo.blog.primes.strategy;

import java.util.BitSet;

/**
 * Algorithm based on sieve of Eratosthenes and used BitSet structure
 * <p/>
 * pros: good performance and friendly to use memory.
 * <p/>
 * cons: for small size structure (avg less than a million records) is slightly slower than the boolean array structure.
 * <p/>
 * author: Anton Krasnov
 */
public class BitSetEratosthenesStrategy implements PrimeCalculationStrategy {

    private static final int FIRST_NUMBER = 2;

    @Override
    public int getPrimesCount(int numberLimit) {
        int primesCount = 0;
        BitSet bitSet = initBitSet(numberLimit);

        int i = FIRST_NUMBER;
        int root = (int) Math.ceil(Math.sqrt(numberLimit));
        while (i < root) {
            if (bitSet.get(i)) {
                primesCount++;
                int multiple = i + i;
                while (multiple <= numberLimit) {
                    bitSet.clear(multiple);
                    multiple += i;
                }
            }
            i++;
        }

        while (i <= numberLimit) {
            if (bitSet.get(i)) {
                primesCount++;
            }
            i++;
        }
        return primesCount;
    }

    private static BitSet initBitSet(int numberLimit) {
        BitSet bitSet = new BitSet(numberLimit + 1);
        for (int i = FIRST_NUMBER; i <= numberLimit; i++) {
            bitSet.set(i);
        }
        return bitSet;
    }

    @Override
    public String getStrategyName() {
        return "BitSet structure with sieve of Eratosthenes algorithm strategy";
    }
}