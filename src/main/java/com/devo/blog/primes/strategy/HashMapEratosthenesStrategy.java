package com.devo.blog.primes.strategy;

import com.devo.blog.primes.PrimesUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * HashMap as the structure is not appropriate for this task and applied only to compare the performance relative to other structures
 *
 * author: Anton Krasnov
 */
public class HashMapEratosthenesStrategy implements PrimeCalculationStrategy {
    @Override
    public int getPrimesCount(int numberLimit) {
        Map<Integer, Integer> naturals = new HashMap<>();
        PrimesUtils.initNaturals(naturals, numberLimit);
        int i = 1;
        while (++i * i < numberLimit) {
            removeMultiples(numberLimit, naturals, i);
        }
        return naturals.size();
    }

    private static void removeMultiples(int numberLimit, Map<Integer, Integer> naturals, int i) {
        for (int number = 2; number <= numberLimit; number++) {
            if (number != i && number % i == 0) {
                naturals.remove(number);
            }
        }
    }

    @Override
    public String getStrategyName() {
        return "HashMap structure with sieve of Eratosthenes algorithm strategy";
    }
}
