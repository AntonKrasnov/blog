package com.devo.blog.primes.strategy;

import com.devo.blog.primes.PrimesUtils;

import java.util.Collection;
import java.util.HashSet;

/**
 * HashSap as the structure is not appropriate for this task and applied only to compare the performance relative to other structures
 * <p>
 * author: Anton Krasnov
 */
public class HashSetEratosthenesStrategy implements PrimeCalculationStrategy {
	@Override
	public int getPrimesCount(int numberLimit) {
		Collection<Integer> naturals = new HashSet<>();
		PrimesUtils.initNaturals(naturals, numberLimit);
		int i = 1;
		while (++i * i < numberLimit) {
			removeMultiples(numberLimit, naturals, i);
		}
		return naturals.size();
	}

	private static void removeMultiples(int numberLimit, Collection<Integer> naturals, int i) {
		for (int number = 2; number <= numberLimit; number++) {
			if (number != i && number % i == 0) {
				naturals.remove(number);
			}
		}
	}

	@Override
	public String getStrategyName() {
		return "HashSet structure with sieve of Eratosthenes algorithm strategy";
	}
}
