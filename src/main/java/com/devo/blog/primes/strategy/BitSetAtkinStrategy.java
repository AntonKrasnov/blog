package com.devo.blog.primes.strategy;

import java.util.BitSet;

/**
 * Algorithm based on sieve of Atkin and used BitSet structure.
 *
 * <p/>
 * pros: excellent performance  (I can't find more effective structure and algorithm on a big range integers). Atkin algorithm is little more efficient than the Sieve of Eratosthenes. Memory friendly.
 * <p/>
 * cons: for small and medium size structure (avg less than a 500 million records) is slightly slower than the boolean array structure.
 * <p/>
 * author: Anton Krasnov
 */
public class BitSetAtkinStrategy implements PrimeCalculationStrategy {

	private static final int FIRST_NUMBER = 2;

	@Override
	public int getPrimesCount(int numberLimit) {
		BitSet bitSet = initBitSet(numberLimit);
		int root = (int) Math.ceil(Math.sqrt((double) numberLimit));

        /* put in candidate primes: integers which have an odd number of representations by certain quadratic forms */
		for (int x = 1; x < root; x++) {
			for (int multiple = 1; multiple < root; multiple++) {
				int n = 4 * x * x + multiple * multiple;
				if (n <= numberLimit && (n % 12 == 1 || n % 12 == 5)) {
					reversBitSetValue(bitSet, n);
				}
				n = 3 * x * x + multiple * multiple;
				if (n <= numberLimit && n % 12 == 7) {
					reversBitSetValue(bitSet, n);
				}
				n = 3 * x * x - multiple * multiple;
				if (x > multiple && n <= numberLimit && n % 12 == 11) {
					reversBitSetValue(bitSet, n);
				}
			}
		}

        /* eliminate composites by sieving, omit multiples of its square */
		for (int i = 5; i <= root; i++) {
			if (bitSet.get(i)) {
				for (int j = i * i; j < numberLimit; j += i * i) {
					bitSet.clear(j);
				}
			}
		}

        /* getting count primes */
		int primeCounts = 0;
		for (int i = FIRST_NUMBER; i <= numberLimit; i++) {
			if (bitSet.get(i)) {
				primeCounts++;
			}
		}
		return primeCounts;
	}

	private static void reversBitSetValue(BitSet bitSet, int n) {
		if (bitSet.get(n)) {
			bitSet.clear(n);
		} else {
			bitSet.set(n);
		}
	}

	private static BitSet initBitSet(int numberLimit) {
		BitSet bitSet = new BitSet(numberLimit + 1);
		bitSet.set(2);
		bitSet.set(3);
		return bitSet;
	}

	@Override
	public String getStrategyName() {
		return "BitSet structure with sieve of Atkin algorithm strategy";
	}
}
