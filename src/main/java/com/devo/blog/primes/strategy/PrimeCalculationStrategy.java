package com.devo.blog.primes.strategy;

/**
 * Base interface of strategy for calculating primes count with with a given upper limit
 *
 * author: Anton Krasnov
 */
public interface PrimeCalculationStrategy {

    /**
     * Method calculating count of primes from natural numbers up to numberLimit.
     * @param numberLimit upper limit of natural series
     * @return count of primes from naturals up to numberLimit
     */
    int getPrimesCount(int numberLimit);

    /**
     * Returns the name of the strategy.
     * @return strategy name
     */
    String getStrategyName();
}

